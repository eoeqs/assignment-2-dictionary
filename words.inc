%include "colon.inc"

section .rodata

colon "grapefruit", first_word
db "is tasty", 0

colon "meadow", second_word
db "camomile meadow is kinda beautiful", 0

colon "pet", third_word
db "doggo", 0

colon "one two", fourth_word
db "three", 0
