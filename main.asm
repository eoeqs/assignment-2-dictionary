%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%include "dict.inc"
%define max_string_size 255

section .data
error_msg: db "fail: word length overflow", 0
not_found_err: db "fail: word not found", 0
new_w: db 0

section .text
global _start

_start:
    mov rdi, new_w
    mov rsi, max_string_size
    call read_line
    test rax, rax
    jz .fail
    mov rdi, rax
    mov rsi, next
    call find_word
    test rax, rax
    je .not_found
    mov rdi, rax
    add rdi, 8    
    inc rdi
    add rdi, rdx
    call print_string
    jmp .exit_pass
    .fail:
        mov rdi, error_msg
        call print_error
        jmp .exit_error
    .not_found:
        mov rdi, not_found_err
        call print_error
        jmp .exit_error
    .exit_pass:
        mov rdi, 0
        call exit
    .exit_error:
            mov rdi, 1
            call exit
