#!/usr/bin/python3

import subprocess

keys = ['pet','grapefruit', 'meadow', 'one two', 'froggo', 'grr' * 256]
output = ['doggo','is tasty', 'camomile meadow is kinda beautiful','three' , '', '']
ret_code = [0,0,0,0,1,1]
error = ['', '', '', '', 'fail: word not found', 'fail: word length overflow']

wrong = 0

for i in range(len(keys)):
    process = subprocess.run(["./program"], input=keys[i], text=True, capture_output=True)
    flag = True
    print(f"Test {i + 1}:")
    print(f"Input: {keys[i]}")
    print(f"Expected Output: {output[i]}")
    print(f"Expected Return Code: {ret_code[i]}")
    print(f"Expected Error: {error[i]}")
    print(f"Actual Output: {process.stdout}")
    print(f"Actual Return Code: {process.returncode}")
    print(f"Actual Error: {process.stderr}")
    
    if process.stdout.strip() != output[i]:
        print(f"Output mismatch")
        wrong += 1

    if process.returncode != ret_code[i]:
        print(f"Return code mismatch")
        wrong += 1

    if process.stderr.strip() != error[i]:
        print(f"Error mismatch")
        wrong += 1

    print()
    
print("Tests failed: ", wrong)
