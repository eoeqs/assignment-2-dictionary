
ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm lib.inc words.inc dict.inc

dict.o: dict.asm lib.inc

words.inc: colon.inc

program: main.o lib.o dict.o 
	$(LD) -o $@ $^

clean:
	rm -f *.o program
test: program
	python3 test.py
.PHONY: clean test
