%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0

%define STDIN 0
%define STDOUT 1
%define STDERR 2


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global read_line


section .text
 
 
; Принимает код возврата и завершает текущий процесс

exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
 
string_length:
    xor rax, rax		
.loop:
    cmp byte [rdi], 0
    je .end
    inc rdi
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в STDOUT

print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, STDOUT
    mov rdx, rax
    mov rax, SYS_WRITE
    syscall
    ret


; Принимает код символа и выводит его в STDOUT

print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp 	
    mov rdx, 1
 
    syscall
    pop rdi
    ret
   

; Переводит строку (выводит символ с кодом 0xA)

print_newline:
    mov di, '\n'
    jmp print_char
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    sub rsp, 32
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    dec rsp
    mov byte[rsp], 0
    .loop:
        xor rdx, rdx
        div r8
        dec rsp
        add rdx, '0'
        mov byte[rsp], dl
        test rax, rax
        jne .loop
    mov rdi, rsp
    push r9
    call print_string
    pop rsp
    add rsp, 32
    ret   
    
   

; Выводит знаковое 8-байтовое число в десятичном формате 

print_int:   
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rax, rax
    xor r8, r8
    .loop:
        mov al, [rdi+r8]
        cmp [rsi+r8], al
        jne .miss
        cmp byte[rdi+r8], 0
        je .hit
        inc r8
        jmp .loop
    .miss:
        xor rax,rax
        ret
    .hit:
        mov rax, SYS_WRITE
        ret


; Читает один символ из STDIN и возвращает его. Возвращает 0 если достигнут конец потока

read_char:
    xor rax, rax
    xor rdi, rdi
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из STDIN, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rsi
    xor r9, r9
    .loop:
        push r8
        push r9
        push rdi
        call read_char
        pop rdi
        pop r9
        pop r8
        cmp rax, 0x20
        je .check
        cmp rax, 0x9
        je .check
        cmp rax, 0xA
        je .check
        cmp rax, 0
        je .pass
        cmp r9, r8
        je .bad
        mov byte[rdi+r9], al
        inc r9
        jmp .loop
        
    .check:
        test r9,r9
        je .loop
    .pass:
        mov rax, rdi
        mov rdx, r9
        mov byte[rdi+r9], 0
        inc r9
        ret
    .bad:
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10
    .loop:
        mov dl, byte[rdi+rcx]
        cmp rdx, '0'
        jl .end
        cmp rdx, '9'
        jg .end
        sub rdx, '0'
        push rdx
        mul r8
        pop rdx
        add rax, rdx
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jnz parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    xor r8,r8
    cmp rax, rdx
    jge .fail
    .loop:
        mov dl, byte[rdi+r8]
        mov byte[rsi+r8], dl
        inc r8
        test rdx,rdx
        jne .loop
        mov rdx, r8
        ret
    .fail:
        xor rax, rax
        ret


print_error:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rax, SYS_WRITE
    mov  rdi, STDERR
    syscall
    jmp print_newline

read_line:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14 
    dec r13
    .loop:
        call read_char
        cmp rax, 0
        je .done
        cmp rax, '\n'
        je .done
        mov byte [r12 + r14], al
        inc r14
        cmp r14, r13
        jg .bad
        jmp .loop
    .done:
        mov byte [r12 + r14], 0
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret
    .bad:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret
