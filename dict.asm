%include "lib.inc"

section .text

global find_word

find_word:
    xor rax, rax
    test rsi, rsi
    jz .found
    .loop:
        push rsi
        push rdi
        add rsi, 8
        call string_equals
        pop rdi
        pop rsi
        test rax, rax
        jnz .found
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop
        ret        
    .found:
        mov rax, rsi
        ret

